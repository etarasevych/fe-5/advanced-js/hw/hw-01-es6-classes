"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary * 3;
  }
}

const employee1 = new Employee('Stepan', 23, 12000);
console.log(employee1);

const programmer1 = new Programmer('Vasya', 25, 12000, ['js', 'c++'])
console.log(programmer1);

const programmer2 = new Programmer('Roma', 28, 15000, ['js', 'c++', 'phyton', 'java'])
console.log(programmer2);

console.log(programmer1.salary)


